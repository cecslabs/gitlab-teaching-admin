## How to use it
```bash
python3 adduser.py -i https://<gitlab url> -k <admin token> -u <marker user name> --id <user username> --projectlimit 2000
```

## example
```bash
python3 adduser.py -i https://gitlab.cecs.anu.edu.au -k yb9b2briKW_aRWwBtMXv -u comp4691-2022-s2-marker --id u1091356 --projectlimit 2000
```
