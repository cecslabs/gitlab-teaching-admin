import requests
import sys
import argparse


def view_user(url, key, username):
	headers = {"Content-Type": "application/json; charset=utf-8", "PRIVATE-TOKEN" : f"{key}"}
	r = requests.get(f"{url}/api/v4/users?username={username}", headers=headers)
	print(r.text)

parser = argparse.ArgumentParser(prog='viewuser.py', description='''List Attributes for User''')

parser.add_argument('--url', '-i', default='https://gitlab.cecs.anu.edu.au', help='GitLab IP Address')
parser.add_argument('--key', '-k', required=True, help='Gitlab API Key')
parser.add_argument('--username', '-u', required=True, help='gitlab username')

args = parser.parse_args()

view_user(args.url, args.key, args.username)

