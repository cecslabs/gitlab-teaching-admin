import requests
import sys
import argparse
import json


def edit_user_project_limit(url, key, username, project_limit):
	headers = {"Content-Type": "application/json; charset=utf-8", "PRIVATE-TOKEN" : f"{key}"}
	r = requests.get(f"{url}/api/v4/users?username={username}", headers=headers)
	data = json.loads(r.text)
	if len(data) == 0:
		print('{"error":"no such user"}')
		return
	id = data[0]["id"]

	payload = {"projects_limit": project_limit}
	r = requests.put(f"{url}/api/v4/users/{id}", headers=headers, json=payload)
	print(r.text)


parser = argparse.ArgumentParser(prog='edituser.py', description='''Edit User project limit''')

parser.add_argument('--url', '-i', default='https://gitlab.cecs.anu.edu.au', help='GitLab IP Address')
parser.add_argument('--key', '-k', required=True, help='Gitlab API Key')
parser.add_argument('--username', '-u', required=True, help='gitlab username')
parser.add_argument('--projects_limit', default=2000 , help='university id i.e., u6076069')


args = parser.parse_args()

edit_user_project_limit(args.url, args.key, args.username, args.projects_limit)

