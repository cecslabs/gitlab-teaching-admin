import requests
import sys
import argparse


def add_user(url, key, username, email, project_limit):
	headers = {"Content-Type": "application/json; charset=utf-8", "PRIVATE-TOKEN" : f"{key}"}
	payload = {"email": f"{email}", "username": f"{username}", "name": f"{username}", "reset_password": True,"projects_limit": project_limit, "skip_confirmation": True}
	r = requests.post(f"{url}/api/v4/users", headers=headers, json=payload)
	print(r.text)


parser = argparse.ArgumentParser(prog='adduser.py', description='''List projects for User''')

parser.add_argument('--url', '-i', default='https://gitlab.cecs.anu.edu.au', help='GitLab IP Address')
parser.add_argument('--key', '-k', required=True, help='Gitlab API Key')
parser.add_argument('--username', '-u', required=True, help='gitlab username')
parser.add_argument('--id', required=True, help='university id i.e., u6076069')
parser.add_argument('--projectlimit', default=2000 , help='university id i.e., u6076069')


args = parser.parse_args()

email = f"{args.id}+{args.username}@anu.edu.au"

add_user(args.url, args.key, args.username, email, args.projectlimit)

